package com.breed.dog.dogbreedapp.service;

import com.breed.dog.dogbreedapp.dto.DogBreedsDTO;
import com.breed.dog.dogbreedapp.entity.Breed;
import com.breed.dog.dogbreedapp.exception.CustomMessage;
import com.breed.dog.dogbreedapp.repository.BreedRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class DogService {

    Logger logger = LoggerFactory.getLogger(DogService.class);

    @Autowired
    BreedRepository breedRepository;
    @Transactional
    public ResponseEntity<CustomMessage> addNewBreed(DogBreedsDTO request) {
        try {
            if (request != null) {
                System.out.println("[PRINT] request: " + request);
                Breed breed = new Breed();
                breed.setCreatedDate(LocalDateTime.now().plusHours(7));
                breed.setName(request.getName());
                System.out.println("[PRINT] breed: " + breed);

                breedRepository.save(breed);
                return new ResponseEntity<>(new CustomMessage("Add new breed successfully!", false, breed), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> updateBreed(DogBreedsDTO request) {
        try {
            if (request != null) {
                Optional<Breed> optionalBreed = breedRepository.findById(request.getId());

                System.out.println("[PRINT] optionalBreed: " + optionalBreed.get().getName());

                optionalBreed.ifPresent(i -> {

                    Breed breedObj = new Breed();

                    breedObj = optionalBreed.get();
                    System.out.println("[PRINT] optionalBreed: " + optionalBreed);
                    breedObj.setModifiedDate(LocalDateTime.now().plusHours(7));
                    breedObj.setName(request.getName());
                    breedRepository.save(breedObj);
                    System.out.println("[PRINT] optionalBreed(NOW): " + breedObj);
                });
                return new ResponseEntity<>(new CustomMessage("Update breed successfully!", false, optionalBreed.get().getName()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> deleteBreed(DogBreedsDTO request) {
        try {
            if (request != null) {
                return new ResponseEntity<>(new CustomMessage("Delete breed successfully!", false, "Breed"), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }
}
