package com.breed.dog.dogbreedapp.service;

import com.breed.dog.dogbreedapp.dto.DogBreedsConsumerDTO;
import com.breed.dog.dogbreedapp.entity.Breed;
import com.breed.dog.dogbreedapp.exception.CustomMessage;
import com.breed.dog.dogbreedapp.repository.BreedRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DogConsumerService {

    Logger logger = LoggerFactory.getLogger(DogConsumerService.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    BreedRepository breedRepository;

    String BASE_URL = "https://dog.ceo/api";
//    String url = "https://dog.ceo/api/breeds/list";
    @Transactional
    public ResponseEntity<CustomMessage> consumeAllDogBreedList() {
        try {
            DogBreedsConsumerDTO dogBreeds = restTemplate.getForObject(BASE_URL + "/breeds/list", DogBreedsConsumerDTO.class);
            for(int i = 0; i < Objects.requireNonNull(dogBreeds).getMessage().size(); i++){
                Breed breed = new Breed();
                List<Breed> identifierBreed = breedRepository.findByBreed(dogBreeds.getMessage().get(i));
                if(identifierBreed != null){
                    breed.setCreatedDate(LocalDateTime.now().plusHours(7));
                    breed.setName(dogBreeds.getMessage().get(i));
                    System.out.println("[PRINT] Saving.. " + dogBreeds.getMessage().get(i));
                    breedRepository.save(breed);
                } else{
                    System.out.println("[PRINT] " + dogBreeds.getMessage().get(i) + " is skipped!");
                }

//                for(int i = 0; i < Objects.requireNonNull(dogBreeds).getMessage().size(); i++){
//                    Optional<Breed> identifierBreed = breedRepository.findByBreed(dogBreeds.getMessage().get(i));
//                    int finalI = i;
//                    identifierBreed.ifPresentOrElse(
//                            o -> {
//                                System.out.println("[PRINT] Breed exist. Skipped!" + o.getName());
//                            },
//                            () -> {
//                                Breed breed = new Breed();
//                                breed.setCreatedDate(LocalDateTime.now().plusHours(7));
//                                breed.setName(dogBreeds.getMessage().get(finalI));
//                                System.out.println("[PRINT] Saving.. " + dogBreeds.getMessage().get(finalI));
//                                breedRepository.save(breed);
//                                System.out.println("Not found");
//                            }
//                    );
//
//                }
            }
//            ResponseEntity<String> dogBreedString = restTemplate.getForEntity(url, String.class);
            return new ResponseEntity<>(new CustomMessage("Successfully consume & push all dogs breed data!", false, dogBreeds.getMessage()), HttpStatus.OK);
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }
}
