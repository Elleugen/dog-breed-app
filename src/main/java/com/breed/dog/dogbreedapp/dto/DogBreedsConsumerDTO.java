package com.breed.dog.dogbreedapp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DogBreedsConsumerDTO {
    private String status;
    private List<String> message;
}
