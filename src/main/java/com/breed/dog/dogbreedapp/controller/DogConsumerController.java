package com.breed.dog.dogbreedapp.controller;

import com.breed.dog.dogbreedapp.service.DogConsumerService;
import com.breed.dog.dogbreedapp.exception.CustomMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/consumer/v1/dog")
@RestController
public class DogConsumerController {

    @Autowired
    DogConsumerService dogConsumerService;

    @GetMapping("/")
    public String dogConsumerController(){
        return "This is dogConsumerController";
    }

    @GetMapping("consume-breed-list")
    public ResponseEntity<CustomMessage> consumeAllDogBreedList() {
        System.out.println("[PRINT] consumeAllDogBreedList..");
        return dogConsumerService.consumeAllDogBreedList();
    }
}
