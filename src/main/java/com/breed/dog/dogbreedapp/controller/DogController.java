package com.breed.dog.dogbreedapp.controller;

import com.breed.dog.dogbreedapp.dto.DogBreedsDTO;
import com.breed.dog.dogbreedapp.exception.CustomMessage;
import com.breed.dog.dogbreedapp.service.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/dog")
@RestController
public class DogController {

    @Autowired
    DogService dogService;
    @GetMapping("/")
    public String helloWorld(){
        return "HelloWorld";
    }

    @PostMapping("add")
    public ResponseEntity<CustomMessage> addNewBreed(@RequestBody DogBreedsDTO request) {
        System.out.println("[PRINT] addNewBreed..");
        return dogService.addNewBreed(request);
    }


    @PutMapping("update")
    public ResponseEntity<CustomMessage> updateBreed(@RequestBody DogBreedsDTO request) {
        System.out.println("[PRINT] updateBreed..");
        return dogService.updateBreed(request);
    }

    @DeleteMapping("delete")
    public ResponseEntity<CustomMessage> deleteBreed(@RequestBody DogBreedsDTO request) {
        System.out.println("[PRINT] deleteBreed..");
        return dogService.deleteBreed(request);
    }
}
