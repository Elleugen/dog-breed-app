package com.breed.dog.dogbreedapp.repository;

import com.breed.dog.dogbreedapp.entity.Breed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BreedRepository extends JpaRepository<Breed, String> {

    @Query("SELECT b from Breed b ")
    List<Breed> findByBreed(String breed);
}
